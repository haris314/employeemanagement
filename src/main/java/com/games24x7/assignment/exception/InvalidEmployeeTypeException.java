package com.games24x7.assignment.exception;

import static com.games24x7.assignment.Constants.INVALID_EMPLOYEE_TYPE_MESSAGE;

public class InvalidEmployeeTypeException extends RuntimeException{
	

	public InvalidEmployeeTypeException() {
		super(INVALID_EMPLOYEE_TYPE_MESSAGE);
	}

}

package com.games24x7.assignment.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

/* For general exceptions */
@Getter
@Setter
public class EmployeeException extends RuntimeException{
	
	private HttpStatus status;
	
	public EmployeeException(String message) {
		super(message);
	}
}

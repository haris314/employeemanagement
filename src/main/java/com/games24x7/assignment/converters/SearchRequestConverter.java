package com.games24x7.assignment.converters;

import static com.games24x7.assignment.Constants.*;

import com.games24x7.assignment.exception.InvalidEmployeeTypeException;
import com.games24x7.assignment.models.SearchRequest;
import com.games24x7.assignment.models.SearchRequestDto;

public class SearchRequestConverter {
	
	public static SearchRequest dtoToEntity(SearchRequestDto dto) {
		SearchRequest entity = new SearchRequest();
		entity.setEid(dto.getEid());
		entity.setName(dto.getName());
		entity.setPhone(dto.getPhone());
		
		if(dto.getType() != null) {
			if(EMPLOYEE_TYPES.indexOf(dto.getType().toUpperCase()) == -1) {
				throw new InvalidEmployeeTypeException();
			}
			entity.setType(EMPLOYEE_TYPES.indexOf(dto.getType().toUpperCase()));
			entity.setTypePresent(true);
		}
		else {
			entity.setTypePresent(false);
		}
		
		return entity;
	}
	
}

package com.games24x7.assignment.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SearchRequest {
	
	private String eid;
	
	private String name;
	
	private int type;
	
	private boolean TypePresent;
	
	private String phone;
}

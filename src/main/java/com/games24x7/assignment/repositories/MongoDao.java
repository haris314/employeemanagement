package com.games24x7.assignment.repositories;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.dao.DuplicateKeyException;

import com.games24x7.assignment.exception.InternalException;
import com.games24x7.assignment.models.Employee;
import com.games24x7.assignment.models.SearchRequest;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class MongoDao {
	
	private final MongoTemplate mongoTemplate;
	
	public MongoDao(@Autowired MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	public boolean doesExist(String eid) {
		log.debug("Checking for existence of eid " + eid);
		try {
			Query query = new Query(Criteria.where("eid").is(eid));
			return mongoTemplate.exists(query, Employee.class);
		}
		catch(Exception e) {
			log.error("Could not check for the existence of employeeid " + eid, e);
			throw new InternalException(e.getMessage(), e);
		}
	}

	public Employee insert(Employee employee) {
		log.debug("Inserting employee " + employee);
		
		try {
			return mongoTemplate.insert(employee);			
		}
		catch(DuplicateKeyException e) {
			throw e;
		}
		catch(Exception e) {
			log.error("Could not insert Employee ", employee, e);
			throw new InternalException(e.getMessage(), e);
		}
	}

	public Employee getEmployee(String eid) {	
		log.debug("Getting employee with eid = " + eid);
		
		try {
			return mongoTemplate.findById(eid, Employee.class);			
		}
		catch(Exception e) {
			log.error("Could not get the Employee with ID = " + eid, e);
			throw new InternalException(e.getMessage(), e);
		}
	}
	

	public void deleteEmployee(String eid) {
		log.debug("Deleting employee with eid = " + eid);
		
		try {
			Query query = new Query(Criteria.where("eid").is(eid));
			mongoTemplate.remove(query, Employee.class);			
		}
		catch(Exception e) {
			log.error("Could not remove the Employee with ID = " + eid);
			throw new InternalException(e.getMessage(), e);
		}
		
	}
	
	/**
	 * Takes the criteria on which search is done
	 * Search results contain the AND of all the parameter values given in the criteria
	 * @param employee
	 * @return
	 */
	public List<Employee> searchSimilar(SearchRequest searchRequest) {
		log.debug("Searching based on search request = " + searchRequest);
		
		try {
			Criteria criteria = new Criteria();
			if(searchRequest.getEid() != null) {
				criteria.and("eid").is(searchRequest.getEid());
			}
			if(searchRequest.getName() != null) {
				criteria.and("name").is(searchRequest.getName());
			}
			if(searchRequest.isTypePresent() == true) {
				criteria.and("type").is(searchRequest.getType());
			}
			if(searchRequest.getPhone() != null) {
				criteria.and("phones").is(searchRequest.getPhone());
			}
			
			return mongoTemplate.find(new Query(criteria), Employee.class);			
		}
		catch(Exception e) {
			log.error("Something went wrong while searching for similar Employees. Given searchRequest: " 
					+ searchRequest, e);
			throw new InternalException(e.getMessage(), e);
		}
	}
	
	
}

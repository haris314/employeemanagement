package com.games24x7.assignment.models;


import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Employee {
	
	@Id
	private String eid;
	
	@NotNull
	private String name;
	
	@NotNull
	private int type;
	
	private List<String> phones;
	
	
}

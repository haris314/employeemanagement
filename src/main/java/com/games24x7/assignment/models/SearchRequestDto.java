package com.games24x7.assignment.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SearchRequestDto {
	
	private String eid;
	
	private String name;
	
	private String type;
	
	private String phone;
}

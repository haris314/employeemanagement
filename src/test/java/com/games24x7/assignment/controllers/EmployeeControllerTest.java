package com.games24x7.assignment.controllers;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static com.games24x7.assignment.Constants.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.games24x7.assignment.exception.KeyDoesNotExistException;
import com.games24x7.assignment.models.Employee;
import com.games24x7.assignment.models.EmployeeDto;
import com.games24x7.assignment.models.SearchRequest;
import com.games24x7.assignment.models.SearchRequestDto;
import com.games24x7.assignment.services.EmployeeService;


@WebMvcTest(EmployeeController.class)
class EmployeeControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private EmployeeService employeeService;
	
	@BeforeEach
	void setUp() throws Exception {	
        
        
	}
	
	/* Tests for addEmployee() */
	
	/* Tests the basic functionality of addEmployee operation */
	@Test
	public void testAddEmployee() throws Exception{
		EmployeeDto employeeDto = new EmployeeDto("1", "One", "SDE", new ArrayList<String>());
		Employee employee = new Employee("1", "One", 0, new ArrayList<String>());
		
		when(employeeService.addEmployee(any(Employee.class))).thenReturn(employee);
		
		mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT)
			.content(new ObjectMapper().writeValueAsString(employeeDto))
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.type").value(employeeDto.getType()))
			.andExpect(MockMvcResultMatchers.jsonPath("eid").value(employeeDto.getEid()));
	}
	
	/* When some fields are null in the given JSON employee object */
	@Test
	public void testAddEmployeeValidation() throws Exception{
		EmployeeDto employeeDto = new EmployeeDto("1", null, null, null);
		Employee employee = new Employee("1", null, 0, null);
		
		when(employeeService.addEmployee(any(Employee.class))).thenReturn(employee);
		
		mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT)
			.content(new ObjectMapper().writeValueAsString(employeeDto))
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isBadRequest());
	}
	
	/* When a used key is used to add a new employee */
	@Test
	public void testAddEmployeeKeyAlreadyExists() throws Exception{
		EmployeeDto employeeDto = new EmployeeDto("1", "One", "SDE", null);

		when(employeeService.addEmployee(any(Employee.class))).thenThrow(DuplicateKeyException.class);
		
		mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT)
			.content(new ObjectMapper().writeValueAsString(employeeDto))
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isBadRequest())
			.andExpect(MockMvcResultMatchers.jsonPath("$.message").value(KEY_ALREADY_EXISTS_MESSAGE));
	}

	
	/* Tests for getEmployee() */
	
	/* Tests the basic functionality of getEmployee operation */
	@Test
	void testGetEmployee() throws Exception{
		EmployeeDto employeeDto = new EmployeeDto("1", "One", "SDE", null);
		Employee employee = new Employee("1", "One", 1, null);
		
		when(employeeService.getEmployee(anyString())).thenReturn(employee);
		
		mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT + "/1")
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employeeDto.getName()))
			.andExpect(MockMvcResultMatchers.jsonPath("eid").value(employeeDto.getEid()));
		
	}
	
	/* Attempts to get an employee who does not exist */
	@Test
	void testGetEmployeeDoesNotExist() throws Exception{
		when(employeeService.getEmployee(anyString())).thenThrow(new KeyDoesNotExistException());
		
		mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT + "/1")
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isBadRequest())
			.andExpect(MockMvcResultMatchers.jsonPath("$.message").value(KEY_DOES_NOT_EXIST_MESSAGE));
	}

	
	/* Tests for updateEmployee() */
	
	/* Tests the basic functionality of updateEMployee operation */
	@Test
	void testUpdateEmployee() throws Exception{
		EmployeeDto employeeDto = new EmployeeDto("1", "One", "SDE", new ArrayList<String>());
		Employee employee = new Employee("1", "One", 1, new ArrayList<String>());
		when(employeeService.updateEmployee(anyString(), any(Employee.class))).thenReturn(employee);
		
		mockMvc.perform(MockMvcRequestBuilders.put(ENDPOINT + "/1")
			.content(new ObjectMapper().writeValueAsString(employeeDto))
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employeeDto.getName()))
			.andExpect(MockMvcResultMatchers.jsonPath("eid").value(employeeDto.getEid()));
	}
	
	/* Tries to update an employee with some null fields */
	@Test
	public void testUpdateEmployeeValidation() throws Exception{
		EmployeeDto employeeDto = new EmployeeDto("1", null, null,  new ArrayList<String>());
		//when(employeeService.updateEmployee(anyString(), any(Employee.class))).thenReturn(employee);
		
		mockMvc.perform(MockMvcRequestBuilders.put(ENDPOINT + "/1")
			.content(new ObjectMapper().writeValueAsString(employeeDto))
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isBadRequest());
	}
	
	/* Tries to update an employee with an already used key */
	@Test
	public void testUpdateEmployeeKeyAlreadyExists() throws Exception{
		EmployeeDto employeeDto = new EmployeeDto("1", "One", "SDE", new ArrayList<String>());
		
		when(employeeService.updateEmployee(anyString(), any(Employee.class))).thenThrow(DuplicateKeyException.class);
		
		mockMvc.perform(MockMvcRequestBuilders.put(ENDPOINT + "/1")
			.content(new ObjectMapper().writeValueAsString(employeeDto))
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isBadRequest())
			.andExpect(MockMvcResultMatchers.jsonPath("$.message").value(KEY_ALREADY_EXISTS_MESSAGE));
	}
	
	
	/* Tests for deleteEmployee() */
	
	/* Tests the basic functionality of deleteEmployee operation */
	@Test
	void testDeleteEmployee() throws Exception{
		
		mockMvc.perform(MockMvcRequestBuilders.delete(ENDPOINT + "/1")
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
	}
	
	/* Tries to delete an employee who does not exist */
	@Test
	void testDeleteEmployeeDoesNotExist() throws Exception{
		doThrow(new KeyDoesNotExistException()).when(employeeService).deleteEmployee(anyString());
		
		mockMvc.perform(MockMvcRequestBuilders.delete(ENDPOINT + "/1")
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isBadRequest())
			.andExpect(MockMvcResultMatchers.jsonPath("$.message").value(KEY_DOES_NOT_EXIST_MESSAGE));
	}

	
	/* Tests for search() */
	
	/* Tests the basic functionality of search operation */
	@Test
	void testGetAll() throws Exception{
		
		List<Employee> employeeList = new ArrayList<Employee>();
		employeeList.add(new Employee("1", "One", 0, null));
		employeeList.add(new Employee("2", "One", 1, null));
		
		when(employeeService.getSearchResults(any(SearchRequest.class))).thenReturn(employeeList);
		
		mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT + "/search")
				.content(new ObjectMapper().writeValueAsString(new SearchRequestDto()))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(employeeList.size()));
	}

}

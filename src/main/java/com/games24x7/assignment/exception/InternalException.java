package com.games24x7.assignment.exception;

public class InternalException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6169626965346085605L;

	public InternalException(String message, Throwable e) {
		super(message, e);
	}

	public InternalException(String message) {
		super(message);
	}
}

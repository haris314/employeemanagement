package com.games24x7.assignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Constants {
	
	private Constants() {
	// restrict instantiation
	}
	
	public static final String ENDPOINT = "/api/employees";
	
	public static final String KEY_ALREADY_EXISTS_MESSAGE = "The given key already exists";
	public static final String KEY_DOES_NOT_EXIST_MESSAGE = "Key does not exist";
	public static final String INVALID_EMPLOYEE_TYPE_MESSAGE = "Given employee type is invalid";
	
	/* A final list to define all the employee types available */
	public static final List<String> EMPLOYEE_TYPES = Collections.unmodifiableList(
		    new ArrayList<String>() {{
		        add("SDE");
		        add("QA");
		        add("BDA");
		        add("MANAGER");
		        add("INTERN");
		    }});
}

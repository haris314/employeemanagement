package com.games24x7.assignment.models.responses;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class ErrorResponse {
	LocalDateTime time;
	String message;
	Object details; // Optional parameter to hold any detail
}

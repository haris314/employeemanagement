package com.games24x7.assignment.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.games24x7.assignment.exception.InternalException;
import com.games24x7.assignment.exception.KeyDoesNotExistException;
import com.games24x7.assignment.models.Employee;
import com.games24x7.assignment.models.SearchRequest;
import com.games24x7.assignment.repositories.MongoDao;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmployeeService {
	
	@Autowired
	private MongoDao repo;
	
	private void checkEidExists(String eid) {
		if(repo.doesExist(eid) == false) {
			log.info("No employee exists with eid = ", eid);
			throw new KeyDoesNotExistException();
		}
	}
	

	public Employee addEmployee(Employee employee){
		
		Employee newEmployee = repo.insert(employee);
		
		log.info("Added a new employee " + employee);
		
		return newEmployee;
	}
	

	public Employee getEmployee(String eid) {
		Employee employee;
		
		employee = repo.getEmployee(eid);				
		
		if(employee == null) {
			log.info("No employee exists with eid = " + eid);
			throw new KeyDoesNotExistException();			
		}
		
		return employee;
	}
	
	
	/**
	 * Adds a new employee or edits the existing employee if eid already exists
	 * If eid and employee.eid are different, 
	 * 		first, the new employee is inserted
	 * 		second, if the insertion is successful, employee with id = eid is deleted
	 */
	public Employee updateEmployee(String eid, Employee employee) {
				
		Employee updatedEmployee = repo.insert(employee);
		if(eid != employee.getEid()) 
			repo.deleteEmployee(eid);
		
		log.info("Updated employee with eid = " + eid + ". Updated employee is " + employee);
		return updatedEmployee;
	}
	

	public void deleteEmployee(String eid) {
		
		this.checkEidExists(eid);
		repo.deleteEmployee(eid);	
		log.info("Deleted emploiyee with eid = " , eid);
	}


	/**
	 * Does a search by in the database based on the given search request
	 * @param employee
	 * @return: List of Employees containing all the searched employees
	 */
	public List<Employee> getSearchResults(SearchRequest searchRequest) {
		
		List<Employee> results = repo.searchSimilar(searchRequest);
		log.info("Found " + results.size() + " results for " + searchRequest);
		
		return results;
	}
}

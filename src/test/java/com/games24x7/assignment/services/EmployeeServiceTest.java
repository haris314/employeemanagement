package com.games24x7.assignment.services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.games24x7.assignment.exception.KeyDoesNotExistException;
import com.games24x7.assignment.models.Employee;
import com.games24x7.assignment.repositories.MongoDao;

import org.springframework.dao.DuplicateKeyException;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class EmployeeServiceTest {
	
	@MockBean
	private MongoDao repo;
	
	@InjectMocks
	private EmployeeService employeeService;
	
	private Employee e1 = new Employee("1", "One", 1, new ArrayList<String>());
	
	/* Tests for addEmployee() */
	
	/* Tests the basic functionality */
	@Test
	public void addEmployeeTest() {
		when(repo.insert(any(Employee.class))).thenReturn(e1);
		
		Employee newEmployee = employeeService.addEmployee(e1);
		
		assertTrue(newEmployee.getEid().equals(e1.getEid()));
		assertTrue(newEmployee.getName().equals(e1.getName()));
	}
	
	/* Tests if DuplicateKeyException is thrown */
	@Test
	public void addEmployeeIdAlreadyExists() {
		when(repo.insert(any(Employee.class))).thenThrow(DuplicateKeyException.class);
		
		assertThrows(DuplicateKeyException.class, () -> employeeService.addEmployee(e1));
	}
	
	/* Tests for getEMployee() */
	
	/* Basic functionality */
	@Test
	public void getEmployeeTest() {
		when(repo.getEmployee(anyString())).thenReturn(e1);
		
		Employee retrievedEmployee = employeeService.getEmployee(e1.getEid());
		
		assertTrue(retrievedEmployee.getEid() == e1.getEid());
	}
	
	/* Test for KeyDoesNotExistException */
	@Test
	public void getEmployeeKeyDoesNotExist() {
		when(repo.doesExist(anyString())).thenReturn(false);
		
		assertThrows(KeyDoesNotExistException.class, () -> employeeService.getEmployee(e1.getEid()));
	}
	
	
	/* Tests for updateEMployee() */
	@Test
	public void updateEmployeeTest() {
		when(repo.insert(any(Employee.class))).thenReturn(e1);
		
		Employee newEmployee = employeeService.updateEmployee(e1.getEid(), e1);
		
		assertTrue(newEmployee.getEid().equals(e1.getEid()));
		assertTrue(newEmployee.getName().equals(e1.getName()));
	}
	
	/* Tests if DuplicateKeyException is thrown */
	@Test
	public void updateEmployeeIdAlreadyExists() {
		when(repo.insert(any(Employee.class))).thenThrow(DuplicateKeyException.class);
		
		assertThrows(DuplicateKeyException.class, () -> employeeService.updateEmployee(e1.getEid(), e1));
	}
	
	
	/* Tests for deleteEmployee() */
	
	/* Test for KeyDoesNotExistException */
	@Test
	public void deleteEmployeeKeyDoesNotExist() {
		when(repo.doesExist(anyString())).thenReturn(false);
		
		assertThrows(KeyDoesNotExistException.class, () -> employeeService.deleteEmployee(e1.getEid()));
	}
	
	

}

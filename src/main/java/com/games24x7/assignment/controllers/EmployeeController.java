package com.games24x7.assignment.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.games24x7.assignment.converters.EmployeeConverter;
import com.games24x7.assignment.converters.SearchRequestConverter;
import com.games24x7.assignment.models.Employee;
import com.games24x7.assignment.models.EmployeeDto;
import com.games24x7.assignment.models.SearchRequestDto;
import com.games24x7.assignment.services.EmployeeService;

import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

@RestController
@RequestMapping("api/employees")
@Slf4j
public class EmployeeController {
	
	
	@Autowired
	private EmployeeService employeeService;
	
	/**
	 * Create 
	 * Accepts an employeeDto object in JSON format
	 * EId is generated randomly if not provided
	 * If name or type is null, you will get an appropriate error response
	 * If the given EId already belongs to an employee, you will get an appropriate error response
	 */
	@PostMapping(path = "")
	public EmployeeDto addEmployee(@RequestBody @Valid EmployeeDto employeeDto){
		log.debug("Adding employee " + employeeDto);
		
		Employee employee = EmployeeConverter.dtoToEntity(employeeDto);
		
		Employee newEmployee = employeeService.addEmployee(employee);
		return EmployeeConverter.entityToDto(newEmployee);
	

	}
	
	/**
	 * Read 
	 * Takes the EId of the employee
	 * If the given EId does not belong to any employee, you will get an appropriate error response
	 */
	@GetMapping(path ="/{eid}")
	public EmployeeDto getEmployee(@PathVariable("eid") String eid) {
		log.debug("Getting employee " + eid);
	
		Employee employee = employeeService.getEmployee(eid);
		return EmployeeConverter.entityToDto(employee);			
		
	}
	
	/**
	 * Update 
	 * Accepts the EId of the employee to be updated and the updated employee object in JSON format
	 * Given EId and updated EId can be different
	 * The updated employeeDto object must be a valid object i.e., name and type must not be null
	 * The updated EId must not belong to an existing employee 
	 */
	@PutMapping(path = "/{eid}")
	public EmployeeDto updateEmployee(@PathVariable("eid") String eid, @RequestBody @Valid EmployeeDto employeeDto) {
		log.debug("Putting employee. Eid = " + eid + ", dto = " + employeeDto);

		Employee employee = EmployeeConverter.dtoToEntity(employeeDto);
		
		Employee updatedEmployee = employeeService.updateEmployee(eid, employee);
		return EmployeeConverter.entityToDto(updatedEmployee);			
	}
	
	/**
	 * Delete 
	 * Accepts the EId of the employee to be deleted
	 * If no employee with the given EId exists, you will get an appropriate error response
	 */
	@DeleteMapping(path = "/{eid}")
	public ResponseEntity<Object> deleteEmployee(@PathVariable String eid) {
		log.debug("Deleting employee " + eid);
	
		employeeService.deleteEmployee(eid);
		
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("success", true);
		body.put("message", "Deleted employee with id = " + String.valueOf(eid));
		
		return new ResponseEntity<>(body, HttpStatus.OK);					
	}
	
	/**
	 * Search 
	 * Accepts an searchRequestDto object in JSON format
	 * Any field can be provided a value or can be left all together
	 * Search can be done based on eid, name, type, or a phone number.
	 * 		If multiple parameters are given, the search is done for the AND of the given parameters.
	 * Returns a list of employees in JSON format of all employees who match all the provided values
	 */
	@PostMapping(path = "/search")
	public List<EmployeeDto> search(@RequestBody SearchRequestDto searchRequest){
		log.debug("Searching for employees with search request = " + searchRequest);
		
		return EmployeeConverter.entityListToDtoList(
				employeeService.getSearchResults(
						SearchRequestConverter.dtoToEntity(searchRequest)
						)
				);			
	}
}

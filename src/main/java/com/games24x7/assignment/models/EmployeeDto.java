package com.games24x7.assignment.models;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDto {	
		
	@Id
	private String eid;
	
	@NotNull
	private String name;
	
	@NotNull
	private String type;
	
	private List<String> phones;
}

package com.games24x7.assignment.exception;
import static com.games24x7.assignment.Constants.*;

public class KeyDoesNotExistException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public KeyDoesNotExistException() {
		super(KEY_DOES_NOT_EXIST_MESSAGE);
	}
}

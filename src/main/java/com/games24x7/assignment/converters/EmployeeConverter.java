package com.games24x7.assignment.converters;

import static com.games24x7.assignment.Constants.*;

import java.util.ArrayList;
import java.util.List;

import com.games24x7.assignment.exception.InternalException;
import com.games24x7.assignment.exception.InvalidEmployeeTypeException;
import com.games24x7.assignment.models.Employee;
import com.games24x7.assignment.models.EmployeeDto;

import lombok.extern.slf4j.Slf4j;

/**
 * A utility class to convert between Employee entity and DTO objects
 *
 */
@Slf4j
public class EmployeeConverter {
	
	
	public static Employee dtoToEntity(EmployeeDto dto) {
		log.debug("Converting " + dto + " to entity");
		
		Employee entity = new Employee();
		entity.setEid(dto.getEid());
		entity.setName(dto.getName());
		entity.setPhones(dto.getPhones());
		
		// Convert from DTO's string type to entity's Integer type
		// Note that type is case insensitive
		int type = EMPLOYEE_TYPES.indexOf(dto.getType().toUpperCase());
		if(type == -1) {
			log.info(dto.getType() + " is not a valid employee type");
			throw new InvalidEmployeeTypeException();
		}
		entity.setType(type);
		
		return entity;
	}
	
	
	public static EmployeeDto entityToDto(Employee entity) {
		log.debug("Converting " + entity + " to dto");
		
		EmployeeDto dto = new EmployeeDto();
		dto.setEid(entity.getEid());
		dto.setName(entity.getName());
		dto.setPhones(entity.getPhones());
		
		// Convert from entity's integer type to string type
		if(entity.getType() >= EMPLOYEE_TYPES.size()) {
			throw new InternalException("Employee entity type is out of range");
		}
		
		String type = EMPLOYEE_TYPES.get(entity.getType());
		dto.setType(type);		
		
		return dto;
	}


	public static List<EmployeeDto> entityListToDtoList(List<Employee> searchResults) {
		List<EmployeeDto> dtoList = new ArrayList<>();
		
		for(Employee employee: searchResults) {
			dtoList.add(EmployeeConverter.entityToDto(employee));
		}
		return dtoList;
	}
}

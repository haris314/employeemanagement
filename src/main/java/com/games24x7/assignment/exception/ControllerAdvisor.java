package com.games24x7.assignment.exception;

import static com.games24x7.assignment.Constants.*;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.games24x7.assignment.models.responses.ErrorResponse;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import org.springframework.dao.DuplicateKeyException;


@ControllerAdvice
@Slf4j
public class ControllerAdvisor extends ResponseEntityExceptionHandler {
	
	
	@ExceptionHandler(DuplicateKeyException.class)
    public ResponseEntity<Object> handleDuplicateKeyException(
        DuplicateKeyException ex, WebRequest request) {

        ErrorResponse response = new ErrorResponse();
        response.setTime(LocalDateTime.now());
        response.setMessage(KEY_ALREADY_EXISTS_MESSAGE);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
	
	
	@ExceptionHandler(KeyDoesNotExistException.class)
    public ResponseEntity<Object> handleKeyDoesNotExistException(
        KeyDoesNotExistException ex, WebRequest request) {

		ErrorResponse response = new ErrorResponse();
        response.setTime(LocalDateTime.now());
        response.setMessage(KEY_DOES_NOT_EXIST_MESSAGE);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
	
	
	@ExceptionHandler(InvalidEmployeeTypeException.class)
    public ResponseEntity<Object> handleKeyDoesNotExistException(
    		InvalidEmployeeTypeException ex, WebRequest request) {

		ErrorResponse response = new ErrorResponse();
        response.setTime(LocalDateTime.now());
        response.setMessage(INVALID_EMPLOYEE_TYPE_MESSAGE);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
		
	
	@ExceptionHandler(EmployeeException.class)
	public ResponseEntity<Object> handleEmployeeManagementException(
			EmployeeException ex, WebRequest request){
	
		log.error(ex.getMessage(), ex);
		
		ErrorResponse response = new ErrorResponse();
		response.setTime(LocalDateTime.now());
        response.setMessage(ex.getMessage());
        
		return new ResponseEntity<>(response, ex.getStatus());
	}
	
	
	@ExceptionHandler(InternalException.class)
	public ResponseEntity<Object> handleEmployeeManagementException(
			InternalException ex, WebRequest request){
	
		log.error(ex.getMessage(), ex);
		
		ErrorResponse response = new ErrorResponse();
		response.setTime(LocalDateTime.now());
        response.setMessage("Some went wrong on the server");
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(Exception.class)
	public void handleGeneralException(Exception ex, WebRequest request) {
		throw new InternalException(ex.getMessage(), ex);
	}

	
	
	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException ex, HttpHeaders headers, 
        HttpStatus status, WebRequest request) {

		ErrorResponse response = new ErrorResponse();
        response.setTime(LocalDateTime.now());
        response.setMessage("Bad parameters");

        response.setDetails(ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList())
        );

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
    
    
}
